﻿using Football.FileReader.Entities;
using Football.FileReader.Interfaces;
using System;
using System.Collections.Generic;
using System.IO;

namespace Football.FileReader
{
    public class CsvFootballFileReader : IFootballFileReader
    {
        private const int EXPECTED_COLUMN_COUNT = 9; 
        private const int COLUMN_INDEX_TEAM = 0;
        private const int COLUMN_INDEX_P = 1;
        private const int COLUMN_INDEX_W = 2;
        private const int COLUMN_INDEX_L = 3;
        private const int COLUMN_INDEX_D = 4;
        private const int COLUMN_INDEX_F = 5;
        private const int COLUMN_INDEX_DASH = 6;
        private const int COLUMN_INDEX_A = 7;
        private const int COLUMN_INDEX_PTS = 8;

        private bool isHeaderRowValid; 

        bool IFootballFileReader.IsHeaderValid
        {
            get { return isHeaderRowValid; } 
        }

        List<FootballFileLine> IFootballFileReader.ReadFile(string path)
        {
            List<FootballFileLine> result = null;

            if (File.Exists(path)) 
            {
                string[] lines = File.ReadAllLines(path);
                result = Parse(lines);
            }

            return result; 
        }

        private List<FootballFileLine> Parse(string[] lines)
        {
            List<FootballFileLine> result = null; 

            if (lines.Length > 0)
            {
                isHeaderRowValid = ValidateHeaderRow(lines[0]);
                if (isHeaderRowValid)
                {
                    result = ParseData(lines, 1);
                }
            }

            return result; 
        }

        private bool ValidateHeaderRow(string line)
        {
            bool result = false; 

            string[] columns = Split(line);

            if (columns != null && columns.Length > 0)
            {
                result = columns[COLUMN_INDEX_TEAM].Trim().ToLower() == "team" &&
                    columns[COLUMN_INDEX_P].Trim().ToLower() == "p" &&
                    columns[COLUMN_INDEX_W].Trim().ToLower() == "w" &&
                    columns[COLUMN_INDEX_L].Trim().ToLower() == "l" &&
                    columns[COLUMN_INDEX_D].Trim().ToLower() == "d" &&
                    columns[COLUMN_INDEX_F].Trim().ToLower() == "f" &&
                    columns[COLUMN_INDEX_DASH].Trim().ToLower() == "-" &&
                    columns[COLUMN_INDEX_PTS].Trim().ToLower() == "pts";
            }

            return result; 
        }

        private List<FootballFileLine> ParseData(string[] lines, int startIndex)
        {
            List<FootballFileLine> result = new List<FootballFileLine>();

            if (lines != null && startIndex > 0 && startIndex < lines.Length)
            {
                for (int i = startIndex; i < lines.Length; i++)
                {
                    var line = lines[i];
                    string[] columns = Split(line);

                    if (columns != null && columns.Length == EXPECTED_COLUMN_COUNT)
                    {
                        result.Add(new FootballFileLine
                        {
                            Team = columns[COLUMN_INDEX_TEAM],
                            P = Convert.ToInt32(columns[COLUMN_INDEX_P]),
                            W = Convert.ToInt32(columns[COLUMN_INDEX_W]),
                            L = Convert.ToInt32(columns[COLUMN_INDEX_L]),
                            D = Convert.ToInt32(columns[COLUMN_INDEX_D]),
                            F = Convert.ToInt32(columns[COLUMN_INDEX_F]),
                            A = Convert.ToInt32(columns[COLUMN_INDEX_A]),
                            Pts = Convert.ToInt32(columns[COLUMN_INDEX_PTS])
                        });
                    }
                }
            }

            return result; 
        }

        private string[] Split(string line)
        {
            string[] result = null;

            if (!string.IsNullOrWhiteSpace(line))
            {
                result = line.Split(',');
            }

            return result; 
        }
    }
}
