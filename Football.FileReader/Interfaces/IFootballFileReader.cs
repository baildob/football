﻿using Football.FileReader.Entities;
using System.Collections.Generic;

namespace Football.FileReader.Interfaces
{
    public interface IFootballFileReader
    {
        bool IsHeaderValid { get; }
        List<FootballFileLine> ReadFile(string path);
    }
}
