﻿using Football.Services.Entities;

namespace Football.Services.Interfaces
{
    public interface IFootballService
    {
        Team GetLowestGoalDifference(string path);
    }
}
