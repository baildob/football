﻿namespace Football.Services.Entities
{
    public class Team
    {
        public string Name { get; set; }
        public string Position { get; set; }
        public int Played { get; set; }
        public int Won { get; set; }
        public int Lost { get; set; }
        public int Drawn { get; set; }
        public int TotalGoalsFor { get; set; }
        public int TotalGoalsAgainst { get; set; }
        public int TotalPoints { get; set; }
        public int GoalDifference { get; set; } 
    }
}
