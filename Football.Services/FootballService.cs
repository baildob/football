﻿using Football.Services.Entities;
using Football.FileReader.Entities;
using Football.FileReader.Interfaces;
using Football.Services.Interfaces;
using System.Collections.Generic;
using System.Linq;

namespace Football.Services
{
    public class FootballService : IFootballService
    {
        public FootballService(IFootballFileReader footballFileReader)
        {
            FootballFileReader = footballFileReader; 
        }

        private IFootballFileReader FootballFileReader { get; set; }

        public Team GetLowestGoalDifference(string path)
        {
            List<Team> teams = new List<Team>();

            List<FootballFileLine> rows = FootballFileReader.ReadFile(path);
            foreach (FootballFileLine row in rows)
            {
                teams.Add(new Team()
                {
                    // row.Team in the format of "7. West_Ham" 
                    Name = row.Team.Split(' ')[1].Replace("_", " ").Trim(),
                    Position = row.Team.Split(' ')[0].Trim(),
                    Played = row.P,
                    Won = row.W,
                    Lost = row.L,
                    Drawn = row.D,
                    TotalGoalsFor = row.F,
                    TotalGoalsAgainst = row.A,
                    TotalPoints = row.Pts,
                    GoalDifference = (row.F > row.A ? row.F - row.A : row.A - row.F)
                });
            }

            // Get the team with the lowest goal difference 
            return teams.OrderBy(a => a.GoalDifference).ToList().FirstOrDefault();
        }
    }
}
