﻿using Football.FileReader;
using Football.FileReader.Interfaces;
using Football.Services;
using Football.Services.Entities;
using Football.Services.Interfaces;
using Microsoft.Extensions.DependencyInjection;
using System;
using System.IO;

namespace Football.ConsoleApp
{
    public class Program
    {
        public static void Main(string[] args)
        {
            var serviceProvider = new ServiceCollection()
                        .AddScoped<IFootballService, FootballService>()
                        .AddScoped<IFootballFileReader, CsvFootballFileReader>()
                        .BuildServiceProvider();

            var footballService = serviceProvider.GetService<IFootballService>();
            Team team = footballService.GetLowestGoalDifference(Path.Combine(Environment.CurrentDirectory, "football.csv"));

            string message = $"The team with the lowest goal difference is {team.Name} with {team.GoalDifference} goal(s).";
            Console.WriteLine(message);
            Console.WriteLine("Press any key to exit...");
            Console.ReadLine(); 
        }
    }
}
