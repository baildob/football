The file football.dat contains the results from the English Premier
League. The columns ‘F’ and ‘A’ contain the total number of goals
scored for and against each team in that season (so Arsenal scored
79 goals against opponents, and had 36 goals scored against them).   

Write a program that validates the structure of the file (columns
only) and prints the name of the team with the smallest difference
in ‘for’ and ‘against’ goals.  

If you wish, you may use football.csv instead of football.dat to get
the data.

