using Football.FileReader.Entities;
using Football.FileReader.Interfaces;
using Football.Services;
using Moq;
using System.Collections.Generic;
using Xunit;

namespace Football.UnitTests
{
    public class FileValidationTests
    {
        [Fact]
        public void ValidateLowestGoalDifferenceTest()
        {
            var lines = new List<FootballFileLine>();
            lines.Add(new FootballFileLine { Team = "1. West Ham", P = 11, W = 10, L = 1, D = 0, F = 20, A = 10, Pts = 30 });
            lines.Add(new FootballFileLine { Team = "2. Man_U", P = 11, W = 9, L = 2, D = 0, F = 30, A = 25, Pts = 27 });
            lines.Add(new FootballFileLine { Team = "3. Liverpool", P = 11, W = 6, L = 5, D = 0, F = 10, A = 20, Pts = 18 });

            var mock = new Mock<IFootballFileReader>();
            mock.Setup(x => x.ReadFile("abc")).Returns(lines);

            var service = new FootballService(mock.Object);
            var team = service.GetLowestGoalDifference("abc");

            Assert.Matches(team.Name, "Man U"); 
        }
    }
}
